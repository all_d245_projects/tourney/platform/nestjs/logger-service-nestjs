import { Module } from '@nestjs/common';
import ConfigModuleConfig from '@/config/modules/config-module.config';
import { MicroserviceResponseInterceptor } from '@/app/common/interceptors/microservice-response.interceptor';
import { LoggerModule } from '@/app/modules/logger/logger.module';

@Module({
  imports: [ConfigModuleConfig, LoggerModule],
  providers: [MicroserviceResponseInterceptor],
})
export class AppModule {}
