import {
  CallHandler,
  ExecutionContext,
  Injectable,
  NestInterceptor,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { APP_INTERCEPTOR } from '@nestjs/core';
import { MicroserviceResposne } from '@/app/common/types/microservice.type';

@Injectable()
export class MicroserviceResponseInterceptor<T>
  implements NestInterceptor<T, MicroserviceResposne>
{
  intercept(
    context: ExecutionContext,
    next: CallHandler,
  ): Observable<MicroserviceResposne> {
    return next.handle().pipe(
      map(data => ({
        message: data?.message || 'ACK',
        result: data?.result || {},
      })),
    );
  }
}

export const MicroserviceResponseInterceptorProvider = {
  provide: APP_INTERCEPTOR,
  useClass: MicroserviceResponseInterceptor,
};
