import { Inject, Injectable } from '@nestjs/common';
import { WinstonLogger } from '@/app/modules/logger/services/winston-logger.service';
import { CreateMicroserviceLogDto } from '@/app/modules/logger/dto/microservice-logger.dto';

export enum Microservice {
  GATEWAY_SERVICE = 'GATEWAY_SERVICE',
  USER_SERVICE = 'USER_SERVICE',
  CORE_SERVICE = 'CORE_SERVICE',
}

export enum MicroserviceLogLevel {
  ERROR = 'error',
  WARNING = 'warning',
  INFO = 'info',
  DEBUG = 'debug',
  HTTP = 'http',
}

@Injectable()
export class MicroserviceLoggerService {
  constructor(
    @Inject('GATEWAY_SERVICE_LOGGER')
    private gatewayServiceLogger: WinstonLogger,
    @Inject('USER_SERVICE_LOGGER') private userServiceLogger: WinstonLogger,
    @Inject('CORE_SERVICE_LOGGER') private coreServiceLogger: WinstonLogger,
  ) {}

  logGatewayService(logData: CreateMicroserviceLogDto): void {
    this.gatewayServiceLogger.log(
      logData.logLevel === 'http' ? 'info' : logData.logLevel,
      logData.timestamp,
      logData.message,
      logData.meta,
    );
  }

  logUserService(logData: CreateMicroserviceLogDto): void {
    this.userServiceLogger.log(
      logData.logLevel,
      logData.timestamp,
      logData.message,
      logData.meta,
    );
  }

  logCoreService(logData: CreateMicroserviceLogDto): void {
    this.coreServiceLogger.log(
      logData.logLevel,
      logData.timestamp,
      logData.message,
      logData.meta,
    );
  }
}
