import { Controller } from '@nestjs/common';
import { EventPattern, Payload } from '@nestjs/microservices';
import {
  Microservice,
  MicroserviceLoggerService,
} from '@/app/modules/logger/services/microservice-logger.service';
import { CreateMicroserviceLogDto } from '@/app/modules/logger/dto/microservice-logger.dto';

@Controller()
export class MicroserviceLoggerController {
  constructor(private readonly loggerService: MicroserviceLoggerService) {}

  @EventPattern('LOGGER_SERVICE.log')
  log(@Payload() logData: CreateMicroserviceLogDto) {
    switch (logData.service) {
      case Microservice.GATEWAY_SERVICE:
        this.loggerService.logGatewayService(logData);
        break;

      case Microservice.USER_SERVICE:
        this.loggerService.logUserService(logData);
        break;

      case Microservice.CORE_SERVICE:
        this.loggerService.logCoreService(logData);
        break;
    }
  }
}
