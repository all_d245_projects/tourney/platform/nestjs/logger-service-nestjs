import {
  CoreServiceLoggerProvider,
  GatewayServiceLoggerProvider,
  UserServiceLoggerProvider,
} from '@/config/modules/winston-module.config';
import { Module } from '@nestjs/common';
import { MicroserviceLoggerService } from '@/app/modules/logger/services/microservice-logger.service';
import { MicroserviceLoggerController } from './controllers/microservice-logger.controller';

@Module({
  providers: [
    GatewayServiceLoggerProvider,
    UserServiceLoggerProvider,
    CoreServiceLoggerProvider,
    MicroserviceLoggerService,
  ],
  exports: [
    GatewayServiceLoggerProvider,
    UserServiceLoggerProvider,
    CoreServiceLoggerProvider,
  ],
  controllers: [MicroserviceLoggerController],
})
export class LoggerModule {}
