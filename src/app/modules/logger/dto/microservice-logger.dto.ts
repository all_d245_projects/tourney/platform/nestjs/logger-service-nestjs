import {
  IsEnum,
  IsISO8601,
  IsNotEmpty,
  IsOptional,
  IsString,
} from 'class-validator';
import {
  Microservice,
  MicroserviceLogLevel,
} from '@/app/modules/logger/services/microservice-logger.service';

export class CreateMicroserviceLogDto {
  @IsNotEmpty()
  @IsEnum(Microservice)
  service: Microservice;

  @IsEnum(MicroserviceLogLevel)
  @IsNotEmpty()
  logLevel: MicroserviceLogLevel;

  @IsISO8601({ strict: true, strictSeparator: true })
  @IsNotEmpty()
  timestamp: string;

  @IsString()
  @IsNotEmpty()
  message: string;

  @IsOptional()
  meta?: any;
}
