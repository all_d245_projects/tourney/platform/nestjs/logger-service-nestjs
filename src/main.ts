import { NestFactory } from '@nestjs/core';
import { AppModule } from '@/app/app.module';
import { ConfigService } from '@nestjs/config';
import { Logger, ValidationPipe } from '@nestjs/common';
import { MicroserviceOptions } from '@nestjs/microservices';
import { AllExceptionsFilter } from './app/common/filter/all-exceptions-filter.filter';
import { AppLogger } from '@/config/modules/winston-module.config';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    bufferLogs: true,
    logger: AppLogger,
  });
  const configService = app.get(ConfigService);
  const natsLoggerServiceConfig = configService.get('nats.loggerService');

  app.useGlobalPipes(
    new ValidationPipe(configService.get('app.globalValidationPipeOptions')),
  );

  app.useGlobalFilters(new AllExceptionsFilter());

  app.connectMicroservice<MicroserviceOptions>(natsLoggerServiceConfig, {
    inheritAppConfig: true,
  });

  await app.startAllMicroservices();
  Logger.log(
    `NATS ready and listening on ${natsLoggerServiceConfig.options.servers}`,
    'AppBootstrap',
  );
}
bootstrap();
