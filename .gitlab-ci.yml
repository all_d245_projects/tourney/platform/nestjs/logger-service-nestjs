stages:
  - prepare
  - build
  - lint
  - test
  - pages

image: node:16.19-alpine3.16

include:
  - template: 'Workflows/MergeRequest-Pipelines.gitlab-ci.yml'

# -------------------- #
# SHARED CONFIG
# -------------------- #
.cache:node_modules:
  before_script:
    - echo "Displaying installed npm packages:"
    - npm list --depth 0 --package-lock-only
  cache:
    key:
      files:
        - package-lock.json
    paths:
      - node_modules/

# -------------------- #
# PREPARE
# -------------------- #
prepare:install_node_modules:
  extends: .cache:node_modules
  stage: prepare
  before_script:
    - "#override"
  script:
    - npm install
  after_script:
    - npm list --depth 0 --package-lock-only
  cache:
    policy: pull-push
  interruptible: true
  needs: []

# -------------------- #
# BUILD
# -------------------- #
build:
  stage: build
  extends: .cache:node_modules
  cache:
    policy: pull
  needs:
    - prepare:install_node_modules
  rules:
    - if: $TEST_ENV
  script:
    - npm run build
    - cp $TEST_ENV .env.test
  artifacts:
    paths:
      - .env.test
      - dist/
    expire_in: 1 day
  interruptible: true

# -------------------- #
# LINT
# -------------------- #
lint:commit:
  extends: .cache:node_modules
  stage: lint
  cache:
    policy: pull
  needs:
   - prepare:install_node_modules
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
  script:
    - echo "$CI_COMMIT_TITLE" | npx --no-install commitlint --verbose
  interruptible: true

lint:source:
  extends: .cache:node_modules
  stage: lint
  needs:
   - prepare:install_node_modules
  cache:
    policy: pull
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
  script:
    - npm run lint:no-fix && npm run format:check
  interruptible: true

# -------------------- #
# TEST
# -------------------- #
test:unit:
  extends: .cache:node_modules
  stage: test
  needs:
    - job: build
      artifacts: true
  script:
    - npm run test:unit:ci
  artifacts:
    paths:
      - coverage/unit
    expire_in: 1 hour
    reports:
      junit: junit.xml
  interruptible: true

test:e2e:
  extends: .cache:node_modules
  stage: test
  needs:
    - job: build
      artifacts: true
  services:
    - name: nats:2.10
      alias: nats
  script:
    - npm run test:e2e:ci
  artifacts:
    paths:
      - coverage/e2e
    expire_in: 1 hour
  interruptible: true

test:coverage:
  extends: .cache:node_modules
  stage: test
  needs:
    - test:unit
    - test:e2e
  script:
    - npm run test:merge
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  coverage: /^Statements\s*:\s*([^%]+)/
  artifacts:
    paths:
      - coverage/merged/lcov-report
    expire_in: 1 hour
    reports:
      coverage_report:
        coverage_format: cobertura
        path: coverage/merged/cobertura-coverage.xml
  interruptible: true

# -------------------- #
# PAGES
# -------------------- #
pages:
  extends: .cache:node_modules
  stage: pages
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  needs:
    - job: test:coverage
      artifacts: true
  script:
    # Prepare public directory
    - mkdir -p .public/assets/css

    # Prepare gitlab pages assets
    - cp ./node_modules/spectre.css/dist/spectre.min.css .public/assets/css
    - cp assets/html/gitlab-pages-index.html .public/index.html
    - cp assets/css/gitlab-pages.css .public/assets/css/gitlab-pages.css

    # Migrate files to public directory
    - echo "1. Move coverage report to Gitlab Pages..."
    - mv coverage/merged/lcov-report .public/coverage

    # Publish public directory
    - mv .public public
  artifacts:
    paths:
      - public
# -----------------------------------------------------------------------------#
# -----------------------------------------------------------------------------#
