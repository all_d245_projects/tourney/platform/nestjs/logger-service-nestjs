import { INestApplication, ValidationPipe } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import { AppModule } from '@/app/app.module';
import {
  ClientProxy,
  ClientProxyFactory,
  MicroserviceOptions,
} from '@nestjs/microservices';

export class AppTestHelper {
  private app: INestApplication;
  private natsClient: ClientProxy;

  public async init(testingModule: TestingModule) {
    this.app = this.createApplication(testingModule);
    await this.app.startAllMicroservices();
    await this.app.init();

    this.natsClient = this.app.get('NATS_SERVICE');

    await this.natsClient.connect();
  }

  public getApp(): INestApplication {
    return this.app;
  }

  public getNatsClient(): ClientProxy {
    return this.natsClient;
  }

  public async terminate() {
    await this.app.close();
    this.natsClient.close();
  }

  static async createE2eTestingModule(): Promise<TestingModule> {
    return await Test.createTestingModule({
      imports: [AppModule],
      providers: [
        {
          provide: 'NATS_SERVICE',
          useFactory: (configService: ConfigService) => {
            const natsLoggerServiceConfig =
              configService.get('nats.loggerService');

            return ClientProxyFactory.create(natsLoggerServiceConfig);
          },
          inject: [ConfigService],
        },
      ],
    }).compile();
  }

  private createApplication(testingModule: TestingModule): INestApplication {
    const app: INestApplication = testingModule.createNestApplication();
    const configService: ConfigService =
      testingModule.get<ConfigService>(ConfigService);
    const natsConfig = configService.get('nats.config');

    app.useGlobalPipes(
      new ValidationPipe(configService.get('app.globalValidationPipeOptions')),
    );

    app.connectMicroservice<MicroserviceOptions>(natsConfig, {
      inheritAppConfig: true,
    });

    return app;
  }
}
