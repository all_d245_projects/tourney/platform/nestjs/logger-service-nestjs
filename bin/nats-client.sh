#!/bin/bash

NATS_SERVER="localhost:4222"
SUBJECT="LOGGER_SERVICE.log"
JSON_PAYLOAD=$(cat <<EOF
{
  "service": "GATEWAY_SERVICE",
  "logLevel": "error",
  "timestamp": "2023-09-16T09:55:57.000Z",
  "message": "Hello World!"
}
EOF
)

# `nats publish` for emitting event messages
# `nats request` for sending request-reply messages
nats publish -s $NATS_SERVER $SUBJECT "$JSON_PAYLOAD"
