# Logger Service - NestJS

## Description
Service responsible for receiving logging events from eaech microservice.

<br/>

## Documentation
Documentation can be found in the gitlab [hosted pages](https://all_d245_projects.gitlab.io/tourney/platform/nestjs/logger-service-nestjs/).

## Installation
1. Install dependencies
    ```bash
    npm install
    ```
2. Install husky (git hooks)
    ```bash
    npm run husky-install
    ```
3. Copy environment variable files
    ```bash
    cp .env.sample .env && cp sample.env .env.test
    ```
<br/>

## Configuration
The project is configured in two primary locations:

**.env file**
The `.env` file located at the root of the project (after copying the `.env.example` file) and it stores configurations which can be modified per each environment.
The available configurations are defined below:  

| Environment Variable                            | Default value                             | Description                                                                |
|-------------------------------------------------|-------------------------------------------|----------------------------------------------------------------------------|
| **NODE_ENV**                                    | `local`                                   | Node environment: valid options include [`local`, `test`]                  |
| **NATS_HOST**                                   | `localhost`                               | NATS host for connecting to the NATS service                               |
| **NATS_PORT**                                   | `4222`                                    | NATS port for connecting to the NATS service                               |


**configuration files**
The configuration files are Nest.js configurations which modify certain aspects of the application.
These files are located in the direction `src/config/registered`.
The following files can be modified:
- `src/config/registered/app.config.ts`: This file includes various app-wide configurations such as environment, global validation behavriour, etc.
- `src/config/registered/nats.config.ts`: This file includes NATS related configurations

## Running the app

```bash
# start application: no env vars will be loaded via cross-env
$ npm run start

# start application in watch mode using local environment
$ npm run start:local

# start application via a REPL environment (useful for testing services)
$ npm run start:repl
```

<br/>

## Test
```bash
# unit tests
npm run test:unit

# unit test coverage
npm run test:unit:cov

# unit test pipeline
npm run test:unit:ci

# e2e tests
npm run test:e2e

# e2e tests coverage
npm run test:e2e:cov

# e2e tests pipeline
npm run test:e2e:ci

# test coverage
# coverage is built by merging both unit and e2e coverage reports. Ensure both unit & e2e coverage reports have been generated before merging
npm run merge-coverage
```
